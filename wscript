#! /usr/bin/env python
# encoding: utf-8

compiler = 'compiler_d'
features = ['d', 'dstlib']
extension = '.d'
target = 'biod'
excludes = ['examples', 'build', 'test']

import os.path as p

def contains(lst, path):
	return(reduce(
		lambda a, b: a or b,
		[p.relpath(l) in p.relpath(path) for l in lst],
		False))

def options(opt):
	opt.load(compiler)

	opt.add_option('', '--debug',
		action = 'store_true',
		dest = 'debug',
		help = 'debug build')

def configure(conf):
	conf.load(compiler)

	import os
	conf.env.append_value('srcs',
		[p.join(p.relpath(d[0]), f)
			for d in os.walk(os.path.abspath("."))
				if not contains(excludes, p.relpath(d[0]))
			for f in d[2]
				if p.splitext(f)[1] == extension])

def build(bld):
	for s in bld.env.srcs:
		bld.objects(
			source = s,
			target = p.splitext(s)[0],
			includes = '.')

	bld.stlib(
		source = '',
		target = target,
		features = features,
		use = [p.splitext(s)[0] for s in bld.env.srcs])
