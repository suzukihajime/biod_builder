# biod_builder

A waf-based build script for BioD library [https://github.com/biod/BioD](https://github.com/biod/BioD).

## usage

	$ cp waf wscript /path/to/BioD/
	$ cd /path/to/BioD
	$ chmod +x waf
	$ ./waf configure build

compile with ldc2 or gdc:

	$ ./waf configure D=ldc2